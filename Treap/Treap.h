/**********************************************
* File: Treap.h
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
*  
**********************************************/
#ifndef TREAP_H
#define TREAP_H

#include <climits>
#include "UniformRandom.h"
#include "dsexceptions.h"
#include <iostream>


using namespace std;

// Treap class
//
// ******************PUBLIC OPERATIONS*********************
// void insert( x )       --> Insert x
// void remove( x )       --> Remove x (unimplemented)
// bool contains( x )     --> Return true if x is present
// T findMin( )  --> Return smallest item
// T findMax( )  --> Return largest item
// bool isEmpty( )        --> Return true if empty; else false
// void makeEmpty( )      --> Remove all items
// void printTree( )      --> Print tree in sorted order
// ******************ERRORS********************************
// Throws UnderflowException as warranted

template <typename T>
class Treap
{
  public:
    /********************************************
    * Function Name  : Treap
    * Pre-conditions :  
    * Post-conditions: none
    *  
    ********************************************/
    Treap( )
    {
        nullNode = new TreapNode;
        nullNode->left = nullNode->right = nullNode;
        nullNode->priority = INT_MAX;
        root = nullNode;
    }

    /********************************************
    * Function Name  : Treap
    * Pre-conditions :  const Treap & rhs 
    * Post-conditions: none
    *  
    ********************************************/
    Treap( const Treap & rhs )
    {
        nullNode = new TreapNode;
        nullNode->left = nullNode->right = nullNode;
        nullNode->priority = INT_MAX;
        root = clone( rhs.root );
    }

    /********************************************
    * Function Name  : ~Treap
    * Pre-conditions :  
    * Post-conditions: none
    *  
    ********************************************/
    ~Treap( )
    {
        makeEmpty( );
        delete nullNode;
    }
    

    /********************************************
    * Function Name  : Treap
    * Pre-conditions :  Treap && rhs 
    * Post-conditions: none
    *  
    ********************************************/
    Treap( Treap && rhs ) : root{ rhs.root }, nullNode{ rhs.nullNode }
    {
        rhs.root = nullptr;
        rhs.nullNode = nullptr;
    }

    
    /**
     * Deep copy.
     */
    /********************************************
    * Function Name  : operator=
    * Pre-conditions :  const Treap & rhs 
    * Post-conditions: Treap &
    *  
    ********************************************/
    Treap & operator=( const Treap & rhs )
    {
        Treap copy = rhs;
        std::swap( *this, copy );
        return *this;
    }
        
    /**
     * Move.
     */
    /********************************************
    * Function Name  : operator=
    * Pre-conditions :  Treap && rhs 
    * Post-conditions: Treap &
    *  
    ********************************************/
    Treap & operator=( Treap && rhs )
    {
        std::swap( root, rhs.root );
        std::swap( nullNode, rhs.nullNode );
        
        return *this;
    }

    /********************************************
    * Function Name  : findMin
    * Pre-conditions :  
    * Post-conditions: const T &
    *  
    ********************************************/
    const T & findMin( ) const
    {
        if( isEmpty( ) )
            throw UnderflowException{ };

        TreapNode *ptr = root;
        while( ptr->left != nullNode )
            ptr = ptr->left;

        return ptr->element;
    }

    /********************************************
    * Function Name  : findMax
    * Pre-conditions :  
    * Post-conditions: const T &
    *  
    ********************************************/
    const T & findMax( ) const
    {
        if( isEmpty( ) )
            throw UnderflowException{ };

        TreapNode *ptr = root;
        while( ptr->right != nullNode )
            ptr = ptr->right;

        return ptr->element;
    }

    /********************************************
    * Function Name  : contains
    * Pre-conditions :  const T & x 
    * Post-conditions: bool
    *  
    ********************************************/
    bool contains( const T & x ) const
    {
        TreapNode *current = root;
        nullNode->element = x;

        for( ; ; )
        {
            if( x < current->element )
                current = current->left;
            else if( current->element < x )
                current = current->right;
            else
                return current != nullNode;
        }
    }

    /********************************************
    * Function Name  : isEmpty
    * Pre-conditions :  
    * Post-conditions: bool
    *  
    ********************************************/
    bool isEmpty( ) const
    {
        return root == nullNode;
    }

    /********************************************
    * Function Name  : printTree
    * Pre-conditions :  
    * Post-conditions: none
    *  
    ********************************************/
    void printTree( ) const
    {
        if( isEmpty( ) )
            cout << "Empty tree" << endl;
        else
            printTree( root );
    }

    /********************************************
    * Function Name  : makeEmpty
    * Pre-conditions :  
    * Post-conditions: none
    *  
    ********************************************/
    void makeEmpty( )
    {
        makeEmpty( root );
    }

    /********************************************
    * Function Name  : insert
    * Pre-conditions :  const T & x 
    * Post-conditions: none
    *  
    ********************************************/
    void insert( const T & x )
    {
        insert( x, root );
    }

    /********************************************
    * Function Name  : insert
    * Pre-conditions :  T && x 
    * Post-conditions: none
    *  
    ********************************************/
    void insert( T && x )
    {
        insert( std::move( x ), root );
    }

    /********************************************
    * Function Name  : remove
    * Pre-conditions :  const T & x 
    * Post-conditions: none
    *  
    ********************************************/
    void remove( const T & x )
    {
        remove( x, root );
    }

  private:
    struct TreapNode
    {
        T element;
        TreapNode *left;
        TreapNode *right;
        int        priority;

        /********************************************
        * Function Name  : TreapNode
        * Pre-conditions :  
        * Post-conditions: none
        *  
        ********************************************/
        TreapNode( ) : left{ nullptr }, right{ nullptr }, priority{ INT_MAX } { }
        
        /********************************************
        * Function Name  : TreapNode
        * Pre-conditions :  const T & e, TreapNode *lt, TreapNode *rt, int pr 
        * Post-conditions: none
        *  
        ********************************************/
        TreapNode( const T & e, TreapNode *lt, TreapNode *rt, int pr )
          : element{ e }, left{ lt }, right{ rt }, priority{ pr }
          { }
        
        /********************************************
        * Function Name  : TreapNode
        * Pre-conditions :  T && e, TreapNode *lt, TreapNode *rt, int pr 
        * Post-conditions: none
        *  
        ********************************************/
        TreapNode( T && e, TreapNode *lt, TreapNode *rt, int pr )
          : element{ std::move( e ) }, left{ lt }, right{ rt }, priority{ pr }
          { }
    };

    TreapNode *root;
    TreapNode *nullNode;
    UniformRandom randomNums;

        // Recursive routines
    /**
     * Internal method to insert into a subtree.
     * x is the item to insert.
     * t is the node that roots the tree.
     * Set the new root of the subtree.
     * (randomNums is a UniformRandom object that is a data member of Treap.)
     */
    /********************************************
    * Function Name  : insert
    * Pre-conditions :  const T & x, TreapNode* & t 
    * Post-conditions: none
    *  
    ********************************************/
    void insert( const T & x, TreapNode* & t )
    {
        if( t == nullNode )
            t = new TreapNode{ x, nullNode, nullNode, randomNums.nextInt( ) };
        else if( x < t->element )
        {
            insert( x, t->left );
            if( t->left->priority < t->priority )
                rotateWithLeftChild( t );
        }
        else if( t->element < x )
        {
            insert( x, t->right );
            if( t->right->priority < t->priority )
                rotateWithRightChild( t );
        }
        // else duplicate; do nothing
    }
    
    /**
     * Internal method to insert into a subtree.
     * x is the item to insert.
     * t is the node that roots the tree.
     * Set the new root of the subtree.
     * (randomNums is a UniformRandom object that is a data member of Treap.)
     */
    /********************************************
    * Function Name  : insert
    * Pre-conditions :  T && x, TreapNode* & t 
    * Post-conditions: none
    *  
    ********************************************/
    void insert( T && x, TreapNode* & t )
    {
        if( t == nullNode )
            t = new TreapNode{ std::move( x ), nullNode, nullNode, randomNums.nextInt( ) };
        else if( x < t->element )
        {
            insert( std::move( x ), t->left );
            if( t->left->priority < t->priority )
                rotateWithLeftChild( t );
        }
        else if( t->element < x )
        {
            insert( std::move( x ), t->right );
            if( t->right->priority < t->priority )
                rotateWithRightChild( t );
        }
        // else duplicate; do nothing
    }

    /**
     * Internal method to remove from a subtree.
     * x is the item to remove.
     * t is the node that roots the tree.
     * Set the new root of the subtree.
     */
    /********************************************
    * Function Name  : remove
    * Pre-conditions :  const T & x, TreapNode * & t 
    * Post-conditions: none
    *  
    ********************************************/
    void remove( const T & x, TreapNode * & t )
    {
        if( t != nullNode )
        {
            if( x < t->element )
                remove( x, t->left );
            else if( t->element < x )
                remove( x, t->right );
            else
            {
                    // Match found
                if( t->left->priority < t->right->priority )
                    rotateWithLeftChild( t );
                else
                    rotateWithRightChild( t );

                if( t != nullNode )      // Continue on down
                    remove( x, t );
                else
                {
                    delete t->left;
                    t->left = nullNode;  // At a leaf
                }
            }
        }
    }

    /********************************************
    * Function Name  : makeEmpty
    * Pre-conditions :  TreapNode * & t 
    * Post-conditions: none
    *  
    ********************************************/
    void makeEmpty( TreapNode * & t )
    {
        if( t != nullNode )
        {
            makeEmpty( t->left );
            makeEmpty( t->right );
            delete t;
        }
        t = nullNode;
    }

    /********************************************
    * Function Name  : printTree
    * Pre-conditions :  TreapNode *t 
    * Post-conditions: none
    *  
    ********************************************/
    void printTree( TreapNode *t ) const
    {
        if( t != nullNode )
        {
            printTree( t->left );
            cout << t->element << endl;
            printTree( t->right );
        }
    }

        // Rotations
    /********************************************
    * Function Name  : rotateWithLeftChild
    * Pre-conditions :  TreapNode * & k2 
    * Post-conditions: none
    *  
    ********************************************/
    void rotateWithLeftChild( TreapNode * & k2 )
    {
        TreapNode *k1 = k2->left;
        k2->left = k1->right;
        k1->right = k2;
        k2 = k1;
    }

    /********************************************
    * Function Name  : rotateWithRightChild
    * Pre-conditions :  TreapNode * & k1 
    * Post-conditions: none
    *  
    ********************************************/
    void rotateWithRightChild( TreapNode * & k1 )
    {
        TreapNode *k2 = k1->right;
        k1->right = k2->left;
        k2->left = k1;
        k1 = k2;
    }

    /********************************************
    * Function Name  : clone
    * Pre-conditions :  TreapNode * t 
    * Post-conditions: TreapNode *
    *  
    ********************************************/
    TreapNode * clone( TreapNode * t ) const
    {
        if( t == t->left )  // Cannot test against nullNode!!!
            return nullNode;
        else
            return new TreapNode{ t->element, clone( t->left ), clone( t->right ), t->priority };
    }
};

#endif
