/**********************************************
* File: English.h
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* Functions for solving the recursive
* Draw English Ruler Problem 
**********************************************/
#ifndef ENGLISH_H
#define ENGLISH_H 

/*******************************************
 * Function: drawTickRow
 * Pre-Conditions: int tickLength
 * Post-Conditions: void
 * Prints a set of tick marks of length tickLength
 *****************************************/
void drawTickRow(int tickLength){
  for(int i = 0; i < tickLength; i++)
    std::cout << "-" ;
  std::cout << std::endl;
}

/*******************************************
 * Function: drawTicks
 * Pre-Conditions: int tickLength
 * Post-Conditions: void
 * Recursively draws an inch of the ruler
 *****************************************/
void drawTicks(int tickLength){	// Draw ticks of given length
  if(tickLength > 0){
    drawTicks(tickLength-1);		// Recursively draw left ticks
    drawTickRow(tickLength);		// Draw Center tick
    drawTicks(tickLength-1);		// Recursively draw right ticks
  }
}

/*******************************************
 * Function: drawRuler
 * Pre-Conditions: int nInches, int majorLength
 * Post-Conditions: void
 * Recursively draws a total # inches where the max tick length is majorLength
 *****************************************/
void drawRuler(int nInches, int majorLength){	
  drawTickRow(majorLength); 	// draw initial 0 ruler marks
  for(int i = 1; i <= nInches; i++){
    drawTicks(majorLength-1);
    drawTickRow(majorLength);
  }
}

#endif
