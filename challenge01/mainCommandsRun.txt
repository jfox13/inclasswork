********COMPILING STRUCT VERSION************
g++ -g -std=gnu++11 -Wpedantic testDateStr.cpp -o testDateStr
 
********COMPILING CLASS VERSION************
g++ -g -std=gnu++11 -Wpedantic testDateCla.cpp dateCla.cpp -o testDateCla			
 
********COMPILING BOTH VERSIONS WITH make all************
g++ -g -std=gnu++11 -Wpedantic testDateStr.cpp -o testDateStr 			
g++ -g -std=gnu++11 -Wpedantic testDateCla.cpp dateCla.cpp -o testDateCla			
 
***************Running testDateStr...******************
Writing to testDateStr.out...
 
***************Running testDataCla...******************
Writing to testDateCla.out
